﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FreeProject.Dtos;
using FreeProject.Services;
using FreeProject.Utils;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace FreeProject.Controllers
{
    public class AccountController : Controller
    {
        protected readonly AccountService _accountService;
        protected readonly UserService _userService;
        public AccountController(AccountService accountService, UserService userService)
        {
            _accountService = accountService;
            _userService = userService;
        }
        // GET: Account
        public ActionResult AccountList(int? page)
        {
            return View( Pagination<AccountDto>.CreateAsync(_accountService.GetDto(), page ?? 1, 1));
        }

        // GET: Account/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        public ActionResult CreateAccount()
        {
            return View();
        }
        // POST: Account/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(AccountDto dto)
        {
            try
            {
                List<AccountDto> dtos = _accountService.GetDto();
                dto.UserId = _userService.GetOneDto().Id;
                dtos.Add(_accountService.CreateDto(dto));
                _accountService.SetDto(dtos);
                return RedirectToAction("AccountList", "Account");
            }
            catch
            {
                return RedirectToAction("AccountList", "Account");
            }
        }

        // GET: Account/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Account/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction(nameof(AccountList));
            }
            catch
            {
                return View();
            }
        }

        // GET: Account/Delete/5
        public ActionResult Delete(int id)
        {
            _accountService.DeleteEntity(id);
            return RedirectToAction(nameof(AccountList));
        }

        // POST: Account/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction(nameof(AccountList));
            }
            catch
            {
                return View();
            }
        }
    }
}