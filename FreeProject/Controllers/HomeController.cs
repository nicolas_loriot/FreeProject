﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using FreeProject.Models;
using FreeProject.Dtos;
using FreeProject.Services;

namespace FreeProject.Controllers
{
    public class HomeController : Controller
    {
        protected readonly UserService _userService;
        protected readonly AccountService _accountService;
        protected readonly OperationsService _operationsService;
        protected readonly OperationTypeService _operationTypeService;
        protected readonly LoginService _loginService;
        protected readonly StatistiqueService _statistiqueService;
        public HomeController(UserService service, 
            AccountService account, 
            OperationsService operationsService, 
            OperationTypeService operationTypeService, 
            LoginService loginService,
            StatistiqueService statistiqueService)
        {
            _userService = service;
            _accountService = account;
            _operationsService = operationsService;
            _operationTypeService = operationTypeService;
            _loginService = loginService;
            _statistiqueService = statistiqueService;
        }
        public IActionResult Index()
        {
            ViewData["Mes comptes"] = "Mes comptes";
            ViewData["Statistiques"] = "Mes statistiques";
            ViewData["Login"] = "Log out";
            return View(_userService.GetOneDto());
        }

        public ActionResult LogOut()
        {
            _operationTypeService.LogOut();
            _operationsService.LogOut();
            _accountService.LogOut();
            _userService.LogOut();
            _loginService.SetConnected(false);
            _loginService.SetFirst(true);
            _statistiqueService.SetListStat(null);
            return RedirectToAction("CreateUser", "User");
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
