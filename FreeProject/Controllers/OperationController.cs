﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FreeProject.Dtos;
using FreeProject.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace FreeProject.Controllers
{
    public class OperationController : Controller
    {
        protected readonly OperationsService _operationsService;
        protected readonly AccountService _accountService;
        protected readonly OperationTypeService _operationTypeService;
        public List<OperationDto> operations;

        public OperationController(OperationsService service, AccountService accountService, OperationTypeService operationTypeService)
        {
            _operationsService = service;
            _accountService = accountService;
            _operationTypeService = operationTypeService;
        }

        public ActionResult OperationList(int id)
        {
            if (id != 0)
            {
                _accountService.SetDto(id);
                _operationsService.SetDto(_accountService.GetDto(id)?.Operations);
                return View(_operationsService.GetDto());
            }
            else
            {
                _operationsService.SetDto(_accountService.GetOneDto()?.Operations);
                return View(_operationsService.GetDto());
            }
           
        }

        public ActionResult CreateOperation()
        {
            return View();
        }
        // GET: Operations/Details/5
        public ActionResult OperationDetails(int id)
        {
            return View(_operationsService.GetDto(id));
        }

        // GET: Operations/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Operations/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(OperationDto dto)
        {
            try
            {
                if (_operationsService.GetDto() == null)
                    _operationsService.SetDto(true);
                List<OperationDto> dtos = _operationsService.GetDto();
                dto.AccountId = _accountService.GetOneDto().Id;
                dto.OperationType = _operationTypeService.GetDto(dto.IdOperationType);
                dtos.Add(_operationsService.CreateDto(dto));
                _operationsService.SetDto(dtos);
                _accountService.SetDto(dto.AccountId);
                return RedirectToAction("OperationList", "Operation");
            }
            catch
            {
                return RedirectToAction("OperationList", "Operation");
            }
        }

        // GET: Operations/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Operations/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction(nameof(OperationList));
            }
            catch
            {
                return View();
            }
        }

        // GET: Operations/Delete/5
        public ActionResult Delete(int id)
        {
            _operationsService.DeleteEntity(id);
            _accountService.RemoveOperationFromAccount(id);
            return RedirectToAction(nameof(OperationList));
        }
        
    }
}