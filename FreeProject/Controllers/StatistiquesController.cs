﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FreeProject.Dtos;
using FreeProject.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace FreeProject.Controllers
{
    public class StatistiquesController : Controller
    {
        protected readonly StatistiqueService _statistiquesService;
        protected readonly AccountService _accountService;
        public StatistiquesController(StatistiqueService statistiqueService, AccountService accountService)
        {
            _statistiquesService = statistiqueService;
            _accountService = accountService;
        }
        // GET: Statistiques
        public ActionResult Statistiques()
        {
            _statistiquesService.SetListStat(_accountService.GetDto());
            List<StatDto> dtos = _statistiquesService.GetStat();
            return View(dtos);
        }

        // GET: Statistiques/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Statistiques/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Statistiques/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(IFormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction(nameof(Statistiques));
            }
            catch
            {
                return View();
            }
        }

        // GET: Statistiques/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Statistiques/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction(nameof(Statistiques));
            }
            catch
            {
                return View();
            }
        }

        // GET: Statistiques/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Statistiques/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction(nameof(Statistiques));
            }
            catch
            {
                return View();
            }
        }
    }
}