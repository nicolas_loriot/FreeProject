﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FreeProject.Dtos;
using FreeProject.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace FreeProject.Controllers
{
    public class UserController : Controller
    {
        protected readonly UserService _userService;
        protected readonly LoginService _loginService;

        public UserController(UserService service, LoginService loginService)
        {
            _userService = service;
            _loginService = loginService;
        }
        // GET: User
        public ActionResult Index(UserDto dto)
        {
            return View(dto);
        }

        public ActionResult LoginView()
        {
            return View();
        }
        public ActionResult Login(UserDto dto)
        {
            dto = _userService.Login(dto);
            if (dto == null)
            {
                _loginService.SetFirst(false);
                return RedirectToAction(nameof (CreateUser));
            }
            else
            {
                _loginService.SetConnected(true);
                _userService.SetDto(dto);
                
                return RedirectToAction("Statistiques", "Statistiques");
            }
        }
        // GET: User/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: User/Create
        public ActionResult CreateUser(UserDto dto)
        {
            if (_loginService.GetUser() == "")
                _loginService.UpdateNoUser(false, "username");
            if (_loginService.GetPassword() == "")
                _loginService.UpdateWrongPassword(false, "password");
            ViewData["no_user"] = _loginService.GetUser();
            ViewData["no_password"] = _loginService.GetPassword();
            return View();
        }

        // POST: User/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(UserDto collection)
        {
            if (collection.FirstName == null || collection.Password == null)
            {
                _loginService.UpdateNoUser(false, "nousername");
                _loginService.UpdateWrongPassword(false, "nopassword");
                return RedirectToAction(nameof(CreateUser));
            }
            else
            {
                UserDto dto = _userService.CreateDto(collection);
                if (dto == null)
                {
                    _loginService.UpdateNoUser(false, "nousername");
                    _loginService.UpdateWrongPassword(false, "nopassword");
                    return RedirectToAction(nameof(CreateUser));
                }
                else
                {
                    _userService.SetDto(dto);
                    _loginService.SetConnected(true);
                    return RedirectToAction("AccountList", "Account");
                }
            }    
        }

        // GET: User/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: User/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction(nameof(CreateUser));
            }
            catch
            {
                return View();
            }
        }

        // GET: User/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: User/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction(nameof(CreateUser));
            }
            catch
            {
                return View();
            }
        }
    }
}