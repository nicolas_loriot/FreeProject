﻿using FreeProjectApi.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FreeProject.Dtos
{
    public class StatDto
    {
        public string Name { get; set; }
        public int NbOperations { get; set; }
        public double Amount { get; set; }
        public double Debit { get; set; }
        public double Credit { get; set; }
    }
}
