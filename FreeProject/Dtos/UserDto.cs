﻿using FreeProjectApi.Interfaces;
using FreeProjectApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FreeProject.Dtos
{
    public class UserDto: IDto
    {
        public UserDto()
        {
            Accounts = new List<AccountDto>();
        }
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string Password { get; set; }
        public List<AccountDto> Accounts { get; set; }
    }
}
