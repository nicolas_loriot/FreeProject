﻿using FreeProjectApi.Interfaces;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace FreeProject.Services
{
    public class AService<TDto> where TDto: IDto, new()
    {
        private TDto dto;
        private List<TDto> dtos;
        protected string route;
        public HttpClient _client;
        public AService()
        {
            this.route = "http://localhost:50379/api/";
            dto = default(TDto);
            dtos = new List<TDto>();
            _client = new HttpClient();
        }
        public virtual ICollection<TDto> GetAllDto()
        {
            List<TDto> dtos = new List<TDto>();
            var resp = _client.GetAsync(route).Result;
            if (resp.IsSuccessStatusCode)
            {
                dtos = JsonConvert.DeserializeObject<List<TDto>>(resp.Content.ReadAsStringAsync().Result);
            }
            return dtos;
        }

        public virtual bool DeleteEntity(int id)
        {
            var rte = route + "delete/" + id;
            var resp = _client.GetAsync(rte).Result;
            if (resp.IsSuccessStatusCode)
                return true;
            return false;
        }

        public virtual void RemoveDto(int id)
        {
            if (dtos != null && dtos.Count > 0)
            {
                List<TDto> dt = new List<TDto>();
                foreach ( var dto in dtos)
                {
                    if (dto.Id != id)
                        dt.Add(dto);
                }
                SetDto(dt);
            }
        }
        public virtual TDto GetUserByName(string name)
        {
            return new TDto();
        }
        public virtual TDto GetDto(int id)
        {
            if (dto == null)
            {
                var resp = _client.GetAsync(route + id).Result;
                if (resp.IsSuccessStatusCode)
                {
                    dto = JsonConvert.DeserializeObject<TDto>(resp.Content.ReadAsStringAsync().Result);
                }
            }
            else
            {
                SetDto(id);
            }
            return dto;
        }
        public virtual TDto GetOneDto()
        {
            return this.dto;
        }
        public virtual List<TDto> GetDto()
        {
            return dtos;
        }

        public virtual void SetDto(List<TDto> dtos)
        {
            this.dtos = dtos;
        }
        public virtual void SetDto(TDto dto)
        {
            this.dto = dto;
        }

        public virtual void SetDto(int id)
        {
            if (dtos != null && dtos.Count > 0)
            {
                dto = dtos.Find(d => d.Id == id);
            }
        }
        public virtual void SetDto(bool list)
        {
            if (list)
                this.dtos = new List<TDto>();
            else
                this.dto = new TDto();
        }
        public virtual TDto CreateDto(TDto dto)
        {
            if (dto != null)
            {
                var content = JsonConvert.SerializeObject(dto);
                var toSend = new StringContent(content, UnicodeEncoding.UTF8, "application/json");
                var response =  _client.PostAsync(route, toSend).Result;
                if (response.IsSuccessStatusCode)
                {
                    dto = JsonConvert.DeserializeObject<TDto>(response.Content.ReadAsStringAsync().Result);
                }
            }
            return dto;
        }

        public virtual void LogOut()
        {
            this.dto = default(TDto);
            this.dtos = null;
        }
    }
}
