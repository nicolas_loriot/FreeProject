﻿using FreeProject.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FreeProject.Services
{
    public class AccountService: AService<AccountDto>
    {
        protected readonly OperationsService _operationsService;
        protected readonly OperationTypeService _operationTypeService;
        public AccountService(OperationsService operationsService, OperationTypeService operationTypeService)
        {
            route += "account/";
            _operationsService = operationsService;
            _operationTypeService = operationTypeService;
        }

        public override void SetDto(AccountDto dto)
        {
            base.SetDto(dto);
            _operationsService.SetDto(dto.Operations);
        }

        public override void SetDto(List<AccountDto> dtos)
        {
            base.SetDto(dtos);
            List<AccountDto> accounts = this.GetDto();
            if (accounts != null && accounts.Count > 0)
            {
                foreach (var a in accounts)
                {
                    _operationsService.SetDto(a.Operations);
                }
            }
        }

        public override bool DeleteEntity(int id)
        {
            bool deleted = base.DeleteEntity(id);
            if (deleted)
            {
                RemoveDto(id);
                foreach (var op in _operationsService.GetDto())
                {
                    if (op.AccountId == id)
                    {
                        _operationsService.RemoveDto(op.Id);
                    }
                }
            }
            return deleted;
        }

        public void RemoveOperationFromAccount(int id)
        {
            List<AccountDto> account = GetDto().ToList();
            int accountId = 0;
            int operationId = 0;
            int j = 0;
            foreach (var a in GetDto())
            {
                int i = 0;
                foreach (var o in a.Operations)
                {
                    if (o.Id == id)
                    {
                        accountId = i;
                        operationId = j;
                    }
                    i++;
                }
                j++;
            }
            account[accountId].Operations.RemoveAt(operationId);
            SetDto(account);
        }
    }
}
