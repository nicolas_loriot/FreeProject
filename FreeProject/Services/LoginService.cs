﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace FreeProject.Services
{
    public class LoginService
    {
        private bool first;
        private bool connected;
        private string noUser;
        private string wrongPassword;
        private string key;
        public LoginService()
        {
            first = true;
            connected = false;
            key = "E546C8DF278CD5931069B522E695D4F2";
        }

        public string GetUser()
        {
            return noUser;
        }
        public string GetPassword()
        {
            return wrongPassword;
        }
        public void SetConnected(bool connected)
        {
            this.connected = connected;
        }
        public void SetFirst(bool first)
        {
            this.first = first;
        }
        public bool GetConnected()
        {
            return this.connected;
        }
        public bool GetFirst()
        {
            return this.first;
        }
        public void UpdateNoUser(bool present, string error)
        {
            if (!present)
                this.noUser = GetErrorMessage(error);
            else
                this.noUser = "";
        }

        public void UpdateWrongPassword(bool ok, string error)
        {
            if (!ok)
                this.wrongPassword = GetErrorMessage(error);
            else
                this.wrongPassword = "";
        }

        public string GetErrorMessage(string error)
        {
            string err = "";
            switch (error)
            {
                case "password":
                    err = this.wrongPassword;
                    break;
                case "username":
                    err = this.noUser;
                    break;
                case "nousername":
                    err = "UserName is requiered";
                    break;
                case "nopassword":
                    err = "Password is requiered";
                    break;
                case "":
                    err = "";
                    break;
                default:
                    err = "Error not found";
                    break;
            }
            return err;
        }
        public string EncryptString(string text)
        {
            var key = Encoding.UTF8.GetBytes(this.key);

            using (var aesAlg = Aes.Create())
            {
                using (var encryptor = aesAlg.CreateEncryptor(key, aesAlg.IV))
                {
                    using (var msEncrypt = new MemoryStream())
                    {
                        using (var csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                        using (var swEncrypt = new StreamWriter(csEncrypt))
                        {
                            swEncrypt.Write(text);
                        }

                        var iv = aesAlg.IV;

                        var decryptedContent = msEncrypt.ToArray();

                        var result = new byte[iv.Length + decryptedContent.Length];

                        Buffer.BlockCopy(iv, 0, result, 0, iv.Length);
                        Buffer.BlockCopy(decryptedContent, 0, result, iv.Length, decryptedContent.Length);

                        return Convert.ToBase64String(result);
                    }
                }
            }
        }

        public string DecryptString(string cipherText)
        {
            var fullCipher = Convert.FromBase64String(cipherText);

            var iv = new byte[16];
            var cipher = new byte[16];

            Buffer.BlockCopy(fullCipher, 0, iv, 0, iv.Length);
            Buffer.BlockCopy(fullCipher, iv.Length, cipher, 0, iv.Length);
            var key = Encoding.UTF8.GetBytes(this.key);

            using (var aesAlg = Aes.Create())
            {
                using (var decryptor = aesAlg.CreateDecryptor(key, iv))
                {
                    string result;
                    using (var msDecrypt = new MemoryStream(cipher))
                    {
                        using (var csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                        {
                            using (var srDecrypt = new StreamReader(csDecrypt))
                            {
                                result = srDecrypt.ReadToEnd();
                            }
                        }
                    }

                    return result;
                }
            }
        }
    }
}
