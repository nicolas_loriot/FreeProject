﻿using FreeProject.Dtos;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FreeProject.Services
{
    public class OperationTypeService: AService<OperationTypeDto>
    {
        public OperationTypeService()
        {
            route += "operationtypes/";
            SetDto(GetAllDto().ToList());
        }

        public List<SelectListItem> GetListItem()
        {
            List<SelectListItem> items = new List<SelectListItem>();
            if (this.GetDto()?.Count > 0)
            {
                List<OperationTypeDto> dtos = this.GetDto();
                foreach (var item in dtos)
                {
                    items.Add(new SelectListItem { Text = item.LabelOperationType, Value = item.Id.ToString() });
                }
            }
            return items;
        }
    }
}
