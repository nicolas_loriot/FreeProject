﻿using FreeProject.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FreeProject.Services
{
    public class OperationsService: AService<OperationDto>
    {
        protected readonly OperationTypeService _operationTypeService;
        public OperationsService(OperationTypeService operationTypeService)
        {
            route += "operations/";
            _operationTypeService = operationTypeService;
        }

        public override OperationDto CreateDto(OperationDto dto)
        {
            OperationDto op = base.CreateDto(dto);
            op.OperationType = _operationTypeService.GetDto(op.IdOperationType);
            return op;
        }
    }
}
