﻿using FreeProject.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FreeProject.Services
{
    public class StatistiqueService
    {
        protected readonly AccountService _accountService;
        protected int nbAccounts;
        protected int amount;
        protected int nbOperations;
        protected List<StatDto> dtos;
        public StatistiqueService(AccountService accountService)
        {
            _accountService = accountService;
            dtos = new List<StatDto>();
        }
        
        public void SetNbAccount()
        {
            if (_accountService.GetDto() != null)
                nbAccounts = _accountService.GetDto().Count();
            else
                nbAccounts = 0;
        }

        public int GetNbAccount()
        {
            return nbAccounts;
        }

        public StatDto GetSolde(AccountDto account, StatDto dto)
        {
            if (account != null && account.Operations.Count > 0)
            {
                foreach (var op in account.Operations)
                {
                    if (op.Amount <= 0)
                        dto.Debit += op.Amount;
                    else
                        dto.Credit += op.Amount;
                }
            }
            return dto;
        }

        public int GetNbOperations(AccountDto account)
        {
            int nbOperation = 0;
            if (account != null && account.Operations.Count > 0)
                nbOperation = account.Operations.Count;
            return nbOperation;
        }

        public void SetListStat(List<AccountDto> accounts)
        {
            if (accounts != null && accounts.Count > 0)
            {
                dtos = new List<StatDto>();
                StatDto lastDto = new StatDto();
                lastDto.Name = "TOTAL";
                foreach (var ac in accounts)
                {
                    StatDto dto = new StatDto();
                    dto = GetSolde(ac, dto);
                    dto.Amount = dto.Credit + dto.Debit;
                    dto.NbOperations = GetNbOperations(ac);
                    dto.Name = ac.Label;
                    dtos.Add(dto);
                    lastDto.Debit += dto.Debit;
                    lastDto.Credit += dto.Credit;
                    lastDto.NbOperations += dto.NbOperations;
                }
                lastDto.Amount = lastDto.Credit + lastDto.Debit;
                dtos.Add(lastDto);
            }
        }

        public List<StatDto> GetStat()
        {
            return this.dtos;
        }
        
    }
}
