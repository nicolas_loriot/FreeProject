﻿using FreeProject.Dtos;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace FreeProject.Services
{
    public class UserService: AService<UserDto>
    {
        protected readonly AccountService _accountService;
        protected readonly LoginService _loginService;

        public UserService(AccountService accountService, LoginService loginService)
        {
            _accountService = accountService;
            _loginService = loginService;
            route += "users/";
        }

        public override UserDto GetDto(int id)
        {
            UserDto user = base.GetDto(id);
            if (user != null)
            {
                _accountService.SetDto(user.Accounts);
            }
            return user;
        }

        public override UserDto GetUserByName(string name)
        {
            string rte = route + "name/" + name;
            UserDto dto = null;
            var resp = _client.GetAsync(rte).Result;
            if (resp.IsSuccessStatusCode)
            {
                dto = JsonConvert.DeserializeObject<UserDto>(resp.Content.ReadAsStringAsync().Result);
            }
            return dto;
        }

        public UserDto Login(UserDto dto)
        {
            this.SetDto(GetUserByName(dto.FirstName));

            if (GetOneDto() == null)
                _loginService.UpdateNoUser(false, "username");
            else
                _loginService.UpdateNoUser(true, "");
            if (dto.Password != _loginService.DecryptString(GetOneDto()?.Password))
            {
                _loginService.UpdateWrongPassword(false, "password");
                SetDto(default(UserDto));
            }
            if (GetOneDto() != null)
            {
                _loginService.UpdateWrongPassword(true, "");
                _accountService.SetDto(GetOneDto().Accounts);
            }
            return GetOneDto();
        }

        public override UserDto CreateDto(UserDto dto)
        {
            UserDto temp = GetUserByName(dto.FirstName);
            if (temp != null)
                dto = null;
            if (dto != null)
                dto.Password = _loginService.EncryptString(dto.Password);
            if (dto != null)
            {
                var content = JsonConvert.SerializeObject(dto);
                var toSend = new StringContent(content, UnicodeEncoding.UTF8, "application/json");
                var response = _client.PostAsync(route, toSend).Result;
                if (response.IsSuccessStatusCode)
                {
                    dto = JsonConvert.DeserializeObject<UserDto>(response.Content.ReadAsStringAsync().Result);
                }
            }
            return dto;
        }
    }
}
