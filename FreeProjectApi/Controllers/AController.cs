﻿using FreeProjectApi.Interfaces;
using FreeProjectApi.Models;
using FreeProjectApi.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FreeProjectApi.Controllers
{
    [Route("api/[controller]")]
    public class AController<TDto, TModel> : Controller where TDto: class, IDto, new()
                                                        where TModel : class, IModels, new()
    {
        protected readonly APIDbContext _context;
        protected readonly AService<TDto, TModel> _service;
        public AController(APIDbContext context, AService<TDto, TModel> service) { 
            _context = context;
            _service = service;
        }
        // GET: api/Users
        [HttpGet]
        public IActionResult Get()
        {
            ICollection<TDto> dtos = null;
            dtos = _service.GetAll();
            if (dtos != null)
                return Ok(_service.GetAll());
            return NoContent();
        }

        // GET: api/Users/5
        [HttpGet("{id}")]
        public IActionResult GetById(int id)
        {
            TDto dto = null;
            dto = _service.GetById(id);
            if (dto != null)
                return Ok(dto);
            return NoContent();
        }

        [HttpGet("name/{name}")]
        public virtual IActionResult GetByName(string name)
        {
            return null;
        }
        // POST: api/Users
        [HttpPost]
        public virtual IActionResult Post([FromBody]TDto dto)
        {
            return Ok(_service.Upsert(dto));
        }

        // PUT: api/Users/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
            throw new NotImplementedException();
        }

        // DELETE: api/ApiWithActions/5
        [HttpGet("delete/{id}")]
        public virtual IActionResult Delete(int id)
        {
            _service.Delete(id);
            return Ok("Entity " + id + " has been deleted");
        }
    }
}

