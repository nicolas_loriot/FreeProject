﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using FreeProjectApi.Dtos;
using FreeProjectApi.Models;
using FreeProjectApi.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace FreeProjectApi.Controllers
{
    [Route("api/[controller]")]
    public class AccountController : AController<AccountDto, Account>
    {
        public AccountController(APIDbContext context, IMapper autoMapper) : base(context, new AccountService(context, autoMapper))
        {

        }
        // GET: api/Account
       
    }
}
