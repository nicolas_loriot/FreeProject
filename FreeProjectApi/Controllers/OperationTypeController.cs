﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using FreeProjectApi.Dtos;
using FreeProjectApi.Models;
using FreeProjectApi.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace FreeProjectApi.Controllers
{
    [Route("api/[controller]")]
    public class OperationTypesController : AController<OperationTypeDto, OperationType>
    {
        public OperationTypesController(APIDbContext context, IMapper autoMapper) : base(context, new OperationTypeService(context, autoMapper))
        {

        }
    }
}
