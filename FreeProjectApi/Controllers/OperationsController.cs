﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using FreeProjectApi.Dtos;
using FreeProjectApi.Models;
using FreeProjectApi.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace FreeProjectApi.Controllers
{
    [Route("api/[controller]")]
    public class OperationsController : AController<OperationDto, Operation>
    {
        public OperationsController(APIDbContext context, IMapper autoMapper) : base(context, new OperationService(context, autoMapper))
        {

        }
    }
}
