﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using FreeProjectApi.Dtos;
using FreeProjectApi.Models;
using FreeProjectApi.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace FreeProjectApi.Controllers
{
    [Route("api/[controller]")]
    public class UsersController : AController<UserDto, User>
    {
        public UsersController(APIDbContext context, IMapper autoMapper) : base(context, new UserService(context, autoMapper))
        {

        }
        [HttpGet("name/{name}")]
        public override IActionResult GetByName(string name)
        {
            UserDto dto = null;
            dto = _service.GetByName(name);
            if (dto != null)
                return Ok(dto);
            return NoContent();
        }
    }
}
