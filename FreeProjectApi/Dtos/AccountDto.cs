﻿using FreeProjectApi.Interfaces;
using FreeProjectApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FreeProjectApi.Dtos
{
    public class AccountDto: IDto
    {
        public AccountDto()
        {
            Operations = new List<OperationDto>();
        }
        public int Id { get; set; }
        public string Label { get; set; }
        public int UserId { get; set; }
        public List<OperationDto> Operations { get; set; }
    }
}
