﻿using FreeProjectApi.Interfaces;
using FreeProjectApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FreeProjectApi.Dtos
{
    public class OperationDto: IDto
    {
        public OperationDto()
        {
            OperationType = new OperationTypeDto();
        }
        public int Id { get; set; }
        public double Amount { get; set; }
        public string Label { get; set; }
        public int IdOperationType { get; set; }
        public OperationTypeDto OperationType { get; set; }
        public int AccountId { get; set; }
    }
}
