﻿using FreeProjectApi.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FreeProjectApi.Dtos
{
    public class OperationTypeDto: IDto
    {
        public int Id { get; set; }
        public string LabelOperationType { get; set; }
    }
}
