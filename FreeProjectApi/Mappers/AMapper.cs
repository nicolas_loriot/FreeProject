﻿using FreeProjectApi.Interfaces;
using FreeProjectApi.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper.Mappers;
using AutoMapper;

namespace FreeProjectApi.Mappers
{
    public class AMapper<TDto, TModel> where TDto: class, IDto, new()
                                       where TModel : class, IModels, new()
    {
        protected readonly APIDbContext _context;
        protected readonly DbSet<TModel> _dbSet;
        protected readonly IMapper _autoMapper;
        protected readonly IMapper tdto;
        protected readonly IMapper tmodel;

        public AMapper(APIDbContext context, IMapper mapper)
        {
            _context = context;
            _dbSet = _context.Set<TModel>();
            _autoMapper = mapper;
        }

        public virtual TDto ToDto(TModel model)
        {
            return new TDto();
        }
        public virtual ICollection<TDto> ToDto(ICollection<TModel> models)
        {
            ICollection<TDto> dtos = new List<TDto>();
            if (models != null && models.Count > 0)
            {
                foreach (var model in models)
                {
                    dtos.Add(ToDto(model));
                }
            }
            return dtos;
        }
        public virtual TModel ToModel(TDto dto)
        {
            return new TModel();
        }
        public virtual ICollection<TModel> ToModel(ICollection<TDto> dtos)
        {
            ICollection<TModel> models = new List<TModel>();
            if (dtos != null && dtos.Count > 0)
            {
                foreach (var dto in dtos)
                {
                    models.Add(ToModel(dto));
                }
            }
            return models;
        }
    }
}
