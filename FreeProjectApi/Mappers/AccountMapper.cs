﻿using AutoMapper;
using FreeProjectApi.Dtos;
using FreeProjectApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FreeProjectApi.Mappers
{
    public class AccountMapper : AMapper<AccountDto, Account>
    {
        private readonly OperationMapper _operationMapper;
        public AccountMapper(APIDbContext context, IMapper mapper): base(context, mapper)
        {
            _operationMapper = new OperationMapper(context, mapper);
        }

        public override AccountDto ToDto(Account model)
        {
            AccountDto account = base.ToDto(model);
            if (model != null)
            {
                account.Id = model.Id;
                account.Label = model.Label;
                account.UserId = model.UserId;
                account.Operations = _operationMapper.ToDto(model.Operations).ToList();
            }
            return account;
        }

        public override Account ToModel(AccountDto dto)
        {
            Account account = base.ToModel(dto);
            if (dto != null)
            {
                account.Id = dto.Id;
                account.Label = dto.Label;
                account.UserId = dto.UserId;
                account.Operations = _operationMapper.ToModel(dto.Operations).ToList();
            }
            return account;
        }
    }
}
