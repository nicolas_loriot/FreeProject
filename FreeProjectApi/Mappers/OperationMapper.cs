﻿using AutoMapper;
using FreeProjectApi.Dtos;
using FreeProjectApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FreeProjectApi.Mappers
{
    public class OperationMapper : AMapper<OperationDto, Operation>
    {
        private readonly OperationTypeMapper _operationTypeMapper;
        public OperationMapper(APIDbContext context, IMapper mapper) : base(context, mapper)
        {
            _operationTypeMapper = new OperationTypeMapper(context, mapper);
        }

        public override OperationDto ToDto(Operation model)
        {
            OperationDto operation = base.ToDto(model);
            if (model != null)
            {
                operation.Id = model.Id;
                operation.Amount = model.Amount;
                operation.Label = model.Label;
                operation.AccountId = model.AccountId;
                operation.OperationType = _operationTypeMapper.ToDto(model.OperationType);
                operation.IdOperationType = model.IdOperationType;
            }
            return operation;
        }

        public override Operation ToModel(OperationDto dto)
        {
            Operation operation = base.ToModel(dto);
            if (dto != null)
            {
                operation.Id = dto.Id;
                operation.Amount = dto.Amount;
                operation.Label = dto.Label;
                operation.AccountId = dto.AccountId;
                operation.OperationType = null;
                operation.IdOperationType = dto.IdOperationType;
            }
            return operation;
        }
    }
}
