﻿using AutoMapper;
using FreeProjectApi.Dtos;
using FreeProjectApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FreeProjectApi.Mappers
{
    public class OperationTypeMapper : AMapper<OperationTypeDto, OperationType>
    {
        public OperationTypeMapper(APIDbContext context, IMapper mapper) : base(context, mapper)
        {

        }

        public override OperationTypeDto ToDto(OperationType model)
        {
            OperationTypeDto dto = base.ToDto(model);
            if (model != null)
            {
                dto.Id = model.Id;
                dto.LabelOperationType = model.LabelOperationType;
            }
            return dto;
        }

        public override OperationType ToModel(OperationTypeDto dto)
        {
            OperationType model = base.ToModel(dto);
            if (dto != null)
            {
                model.Id = dto.Id;
                model.LabelOperationType = dto.LabelOperationType;
            }
            return model;
        }
    }
}
