﻿using AutoMapper;
using FreeProjectApi.Dtos;
using FreeProjectApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FreeProjectApi.Mappers
{
    public class UserMapper : AMapper<UserDto, User>
    {
        protected readonly AccountMapper _accountMapper;
        public UserMapper(APIDbContext context, IMapper mapper) : base(context, mapper)
        {
            _accountMapper = new AccountMapper(context, mapper);
        }

        public override UserDto ToDto(User model)
        {
            UserDto dto = base.ToDto(model);
            if (model != null)
            {
                dto.Id = model.Id;
                dto.FirstName = model.FirstName;
                dto.Password = model.Password;
                dto.Accounts = _accountMapper.ToDto(model.Accounts).ToList();
            }
            return dto;
        }

        public override User ToModel(UserDto dto)
        {
            User model = base.ToModel(dto);
            if (dto != null)
            {
                model.Id = dto.Id;
                model.FirstName = dto.FirstName;
                model.Password = dto.Password;
                model.Accounts = _accountMapper.ToModel(dto.Accounts).ToList();
            }
            return model;
        }
    }
}
