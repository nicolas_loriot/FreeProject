﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace FreeProjectApi.Migrations
{
    public partial class remove_attributes : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Users_Civility_CivilityId",
                table: "Users");

            migrationBuilder.DropTable(
                name: "Civility");

            migrationBuilder.DropIndex(
                name: "IX_Users_CivilityId",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "CivilityId",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "Email",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "LastName",
                table: "Users");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "CivilityId",
                table: "Users",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "Email",
                table: "Users",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "LastName",
                table: "Users",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Civility",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    LabelCivility = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Civility", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Users_CivilityId",
                table: "Users",
                column: "CivilityId");

            migrationBuilder.AddForeignKey(
                name: "FK_Users_Civility_CivilityId",
                table: "Users",
                column: "CivilityId",
                principalTable: "Civility",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
