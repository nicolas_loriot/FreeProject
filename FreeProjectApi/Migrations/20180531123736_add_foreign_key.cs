﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace FreeProjectApi.Migrations
{
    public partial class add_foreign_key : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Operations_OperationsTypes_IdOperationType",
                table: "Operations");

            migrationBuilder.DropIndex(
                name: "IX_Operations_IdOperationType",
                table: "Operations");

            migrationBuilder.AddColumn<int>(
                name: "OperationTypeId",
                table: "Operations",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Operations_OperationTypeId",
                table: "Operations",
                column: "OperationTypeId");

            migrationBuilder.AddForeignKey(
                name: "FK_Operations_OperationsTypes_OperationTypeId",
                table: "Operations",
                column: "OperationTypeId",
                principalTable: "OperationsTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Operations_OperationsTypes_OperationTypeId",
                table: "Operations");

            migrationBuilder.DropIndex(
                name: "IX_Operations_OperationTypeId",
                table: "Operations");

            migrationBuilder.DropColumn(
                name: "OperationTypeId",
                table: "Operations");

            migrationBuilder.CreateIndex(
                name: "IX_Operations_IdOperationType",
                table: "Operations",
                column: "IdOperationType");

            migrationBuilder.AddForeignKey(
                name: "FK_Operations_OperationsTypes_IdOperationType",
                table: "Operations",
                column: "IdOperationType",
                principalTable: "OperationsTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
