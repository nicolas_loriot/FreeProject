﻿using FreeProjectApi.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace FreeProjectApi.Models
{
    public class Account: IModels
    {
        public Account()
        {
            Operations = new List<Operation>();
        }
        public int Id { get; set; }
        public string Label { get; set; }
        [ForeignKey("UserId")]
        public int UserId { get; set; }
        public virtual List<Operation> Operations { get; set; }
    }
}
