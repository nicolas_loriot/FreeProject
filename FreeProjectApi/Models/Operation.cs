﻿using FreeProjectApi.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace FreeProjectApi.Models
{
    public class Operation : IModels
    {
        public Operation()
        {
            OperationType = new OperationType();
        }
        public int Id { get; set; }
        public string Label { get; set; }
        public double Amount { get; set; }
        [ForeignKey("IdOperationType")]
        public int IdOperationType { get; set; }
        public OperationType OperationType { get; set; }
        [ForeignKey("AccountId")]
        public int AccountId { get; set; }
    }
}
