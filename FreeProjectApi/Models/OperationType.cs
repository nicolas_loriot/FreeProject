﻿using FreeProjectApi.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FreeProjectApi.Models
{
    public class OperationType : IModels
    {
        public int Id { get; set; }
        public string LabelOperationType { get; set; }
    }
}
