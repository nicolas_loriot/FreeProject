﻿using FreeProjectApi.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace FreeProjectApi.Models
{
    public class User : IModels
    {
        public User()
        {
            Accounts = new List<Account>();
        }
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string Password { get; set; }
        public virtual List<Account> Accounts { get; set; }
    }
}
