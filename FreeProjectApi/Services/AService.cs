﻿using AutoMapper;
using FreeProjectApi.Interfaces;
using FreeProjectApi.Mappers;
using FreeProjectApi.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FreeProjectApi.Services
{
    public class AService<TDto, TModel> : IAService<TDto, TModel> where TModel : class, IModels, new()
                                                                  where TDto : class, IDto, new()
    {
        protected readonly APIDbContext _context;
        protected readonly DbSet<TModel> _dbSet;
        protected readonly AMapper<TDto, TModel> _mapper;

        public AService(APIDbContext context, AMapper<TDto, TModel> mapper)
        {
            _context = context;
            _dbSet = _context.Set<TModel>();
            _mapper = mapper;
        }

        public virtual void Delete(int id)
        {
            TModel model = _dbSet.FirstOrDefault(c => c.Id == id);
            if (model != null)
            {
                _dbSet.Remove(model);
                _context.SaveChanges();
            }
        }

        public virtual ICollection<TDto> GetAll()
        {
            List<TModel> models = _dbSet.ToList();
            List<TDto> dtos = new List<TDto>();
            if (models != null && models.Count > 0)
            {
                dtos = _mapper.ToDto(models).ToList();
            }
            return dtos;
        }

        public virtual TDto GetById(int id)
        {
            TModel model = _dbSet.FirstOrDefault(c => c.Id == id);
            TDto dto = null;
            if (model != null)
            {
                dto = new TDto();
                dto = _mapper.ToDto(model);
            }
            return dto;
        }

        public virtual TDto Upsert(TDto dto)
        {
            TModel model = new TModel();
            if (dto != null)
            {
                model = _dbSet.FirstOrDefault(e => e.Id == dto.Id);
                model = _mapper.ToModel(dto);
                _dbSet.Add(model);
                _context.SaveChanges();
            }
            return _mapper.ToDto(model);
        }

        public virtual TDto GetByName(string name)
        {
            return null;
        }
    }
}
