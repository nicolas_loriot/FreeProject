﻿using AutoMapper;
using FreeProjectApi.Dtos;
using FreeProjectApi.Mappers;
using FreeProjectApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FreeProjectApi.Services
{
    public class AccountService: AService<AccountDto, Account>
    {
        private OperationService _operationService;
        public AccountService(APIDbContext context, IMapper autoMapper) : base(context, new AccountMapper(context, autoMapper))
        {
            _operationService = new OperationService(context, autoMapper);
        }

        public override ICollection<AccountDto> GetAll()
        {
            List<AccountDto> dtos = base.GetAll()?.ToList();
            if (dtos != null)
            {
                foreach (var dt in dtos)
                {
                    dt.Operations = _operationService.GetAll()?.Where(o => o.AccountId == dt.Id)?.ToList();
                }
            }
            return dtos;
        }

        public override AccountDto GetById(int id)
        {
            AccountDto dto = base.GetById(id);
            if (dto != null)
            {
                dto.Operations = _operationService.GetAll().Where(o => o.AccountId == dto.Id).ToList();
            }
            return dto;
        }
    }
}
