﻿using FreeProjectApi.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FreeProjectApi.Services
{
    interface IAService<TDto, TModels> where TDto: class, IDto, new()
                                       where TModels: class, IModels, new()
    {
        ICollection<TDto> GetAll();
        TDto Upsert(TDto dto);
        TDto GetById(int id);
        void Delete(int id);
    }
}
