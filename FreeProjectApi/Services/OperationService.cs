﻿using AutoMapper;
using FreeProjectApi.Dtos;
using FreeProjectApi.Mappers;
using FreeProjectApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FreeProjectApi.Services
{
    public class OperationService: AService<OperationDto, Operation>
    {
        protected readonly OperationTypeService _operationTypeService;
        public OperationService(APIDbContext context, IMapper autoMapper) : base(context, new OperationMapper(context, autoMapper))
        {
            _operationTypeService = new OperationTypeService(context, autoMapper);
        }

        public override ICollection<OperationDto> GetAll()
        {
            List<OperationDto> dtos = base.GetAll()?.ToList();
            if (dtos != null)
            {
                foreach (var dto in dtos)
                {
                    dto.OperationType = _operationTypeService.GetById(dto.IdOperationType);
                }
            }
            return dtos;
        }

        public override OperationDto GetById(int id)
        {
            OperationDto dto = base.GetById(id);
            if (dto != null)
            {
                dto.OperationType = _operationTypeService.GetById(dto.IdOperationType);
            }
            return dto;
        }
    }
}
