﻿using AutoMapper;
using FreeProjectApi.Dtos;
using FreeProjectApi.Mappers;
using FreeProjectApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FreeProjectApi.Services
{
    public class OperationTypeService: AService<OperationTypeDto, OperationType>
    {
        public OperationTypeService(APIDbContext context, IMapper autoMapper) : base(context, new OperationTypeMapper(context, autoMapper))
        {
            
        }
    }
}
