﻿using AutoMapper;
using FreeProjectApi.Dtos;
using FreeProjectApi.Mappers;
using FreeProjectApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FreeProjectApi.Services
{
    public class UserService: AService<UserDto, User>
    {
        protected readonly AccountService _accountService;

        public UserService(APIDbContext context, IMapper autoMapper) : base(context, new UserMapper(context, autoMapper))
        {
            _accountService = new AccountService(context, autoMapper);
        }

        public override ICollection<UserDto> GetAll()
        {
            List<UserDto> dtos = base.GetAll().ToList();
            if (dtos != null)
            {
                foreach (var dto in dtos)
                {
                    dto.Accounts = _accountService.GetAll().Where(a => a.UserId == dto.Id).ToList();
                }
            }
            return dtos;
        }

        public override UserDto GetById(int id)
        {
            UserDto dto = base.GetById(id);
            if (dto != null)
            {
                dto.Accounts = _accountService.GetAll().Where(a => a.UserId == id).ToList();
            }
            return dto;
        }

        public override UserDto GetByName(string name)
        {
            UserDto dto = _mapper.ToDto(_dbSet.FirstOrDefault(u => u.FirstName == name));
            if (dto.Id == 0)
                return null;
            else
                dto.Accounts = _accountService.GetAll().Where(a => a.UserId == dto.Id).ToList();
            return dto;
        }
    }
}
